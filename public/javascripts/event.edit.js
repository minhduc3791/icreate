$( document ).ready(function() {
    var event = {},
        event_id = $('#jsonId').data('data'),
        countEventBlock = 0, countTicket = 0, countVoucherGroup = 0, countBanners = 0,
        jsonBanners = $('#jsonBanners').data('data'), jsonEventBlocks = $('#jsonEventBlocks').data('data'), jsonTickets = $('#jsonTickets').data('data'), jsonVoucherGroups = $('#jsonVoucherGroups').data('data'),
        tickets = [], voucher_groups = [], banners = [], eventBlocks = [],
        removedEventBlocks = [], removedTickets = [], removedVoucherGroups = [], removedBanners = [];

    function setTime(start_item, end_item) {
        $(start_item).datetimepicker();
        $(end_item).datetimepicker({
            useCurrent: false //Important! See issue #1075
        });

        $(start_item).on("dp.change", function (e) {
            $(end_item).data("DateTimePicker").minDate(e.date);
            //$(start_item).data("DateTimePicker").hide();
            //$(end_item).data("DateTimePicker").show();
        });

        $(end_item).on("dp.change", function (e) {
            //$(end_item).data("DateTimePicker").hide();
        });
    }

    function setSelect(id, val, text) {
        $('select#' + id).append( $("<option>")
            .val(val)
            .html(text)
        );
    }

    setTime('#eventSTime', '#eventETime');
    setTime('#addTicketSTime', '#addTicketETime');
    setTime('#editTicketSTime', '#editTicketETime');
    setTime('#addVoucherGroupSTime', '#addVoucherGroupETime');
    setTime('#editVoucherGroupSTime', '#editVoucherGroupETime');

    $("#eventSTime").data("DateTimePicker").defaultDate(new Date($("#eventSTime > input").data('time')));
    $("#eventETime").data("DateTimePicker").defaultDate(new Date($("#eventETime > input").data('time')));

    function addTicketItem(ticket) {
        $("#ticket").append("<div id=\"removeTicket-" + ticket.ticket_id + "\" class=\"checkbox\">\n" +
            "        <label  class=\"col-lg-6\">\n" + ticket.name +
            "        </label>\n" +
            "        <button type=\"button\" class=\"btn btn-danger btn-xs removeTicket\">\n" +
            "        <i class=\"fa fa-minus\"></i>\n" +
            "        </button>\n" +
            "        <button type=\"button\" class=\"btn btn-warning btn-xs editTicket\" data-toggle=\"modal\" data-target=\"#editTicket\">\n" +
            "        <i class=\"fa fa-edit\"></i>\n" +
            "        </button>\n" +
            "        </div>");


        countTicket++;

        //reset value for next add
        $("#addTicketStatus").prop('checked', true).change();
        $("#addTicketNoCOD").prop('checked', false).change();
        $("#addTicketName").val('');
        $("#addTicketDisplayName").val('');
        $("#addTicketDisplayNameEn").val('');
        $("#addTicketPrice").val('');
        $("#addTicketMinbuy").val('');
        $("#addTicketMaxbuy").val('');
        $("#addTicketQuantity").val('');
        $("#addTicketSTime").data("DateTimePicker").clear();
        $("#addTicketETime").data("DateTimePicker").clear();
        $("#addTicketNote").val('');
        $("#addTicketNoteEn").val('');

        //have to put removed id into array, then re-process when submit
        //remove item shown
        $(".removeTicket").off("click").on("click", function () {
            var $parent = $(this).parent();
            var id = $parent.attr("id").split("-")[1];
            $parent.remove();
            removedTickets.push(id);
        });

        //init data for edit
        $(".editTicket").off("click").on("click", function () {
            var $parent = $(this).parent();
            var id = $parent.attr("id").split("-")[1];
            var ticket = tickets[id];
            $('#editTicketId').data("id", id);
            $("#editTicketStatus").prop('checked', ticket.status).change();
            $("#editTicketNoCOD").prop('checked', ticket.no_COD).change();
            $("#editTicketNoPromotion").prop('checked', ticket.no_promotion).change();
            $("#editTicketName").val(ticket.name);
            $("#editTicketDisplayName").val(ticket.display_name);
            $("#editTicketDisplayNameEn").val(ticket.display_name_en);
            $("#editTicketPrice").val(ticket.price);
            $("#editTicketMinbuy").val(ticket.min_buy);
            $("#editTicketMaxbuy").val(ticket.max_buy);
            $("#editTicketQuantity").val(ticket.quantity);
            $("#editTicketSTime").data("DateTimePicker").defaultDate(new Date(ticket.start_time));
            $("#editTicketETime").data("DateTimePicker").defaultDate(new Date(ticket.end_time));
            $("#editTicketNote").val(ticket.note);
            $("#editTicketNoteEn").val(ticket.note_en);
        });

        //save edited
        $("#saveEditTicket").off('click').on('click', function () {
            var id = $('#editTicketId').data("id");
            var ticket = tickets[id];
            ticket.ticket_id = id;
            ticket.event_id = event_id;
            ticket.enable = $("#editTicketStatus").prop('checked') ? 1 : 0;
            ticket.no_COD = $("#editTicketNoCOD").prop('checked') ? 1 : 0;
            ticket.no_promotion = $("#editTicketNoPromotion").prop('checked') ? 1 : 0;
            ticket.name = $("#editTicketName").val();
            ticket.display_name = $("#editTicketDisplayName").val();
            ticket.display_name_en = $("#editTicketDisplayNameEn").val();
            ticket.price = parseInt($("#editTicketPrice").val());
            ticket.min_buy = parseInt($("#editTicketMinbuy").val());
            ticket.max_buy = parseInt($("#editTicketMaxbuy").val());
            ticket.quantity = parseInt($("#editTicketQuantity").val());
            ticket.start_time = new Date($('#editTicketSTime').data('date')).getTime();
            ticket.end_time = new Date($('#editTicketETime').data('date')).getTime();
            ticket.note = $("#editTicketNote").val();
            ticket.note_en = $("#editTicketNoteEn").val();

            $("#editTicketSTime").data("DateTimePicker").clear();
            $("#editTicketETime").data("DateTimePicker").clear();

            $('#removeTicket-' + id + ' > label').text(ticket.name);
        });
    }

    function addEventBlockItem(eventBlock) {
        //add element to show
        $("#eventBlock").append("<div id=\"removeEventBlock-" + countEventBlock + "\" class=\"checkbox\">\n" +
            "        <label class=\"col-lg-6\">\n" + eventBlock.short_name +
            "        </label>\n" +
            "        <button type=\"button\" class=\"btn btn-danger btn-xs removeEventBlock\">\n" +
            "        <i class=\"fa fa-minus\"></i>\n" +
            "        </button>\n" +
            "        <button type=\"button\" class=\"btn btn-warning btn-xs editEventBlock\" data-toggle=\"modal\" data-target=\"#editEventBlock\">\n" +
            "        <i class=\"fa fa-edit\"></i>\n" +
            "        </button>\n" +
            "        </div>");

        countEventBlock++;

        //reset value for next add
        $("#addEventBlockShortName").val('') ;
        $("#addEventBlockShortNameEn").val('');
        $("#addEventBlockTitle").val('') ;
        $("#addEventBlockImage").val('');
        $("#addEventBlockContent").val('');
        $("#addEventBlockContentEn").val('');

        //have to put removed id into array, then re-process when submit
        //remove item shown
        $(".removeEventBlock").off("click").on("click", function () {
            var $parent = $(this).parent();
            var id = parseInt($parent.attr("id").split("-")[1]);
            $parent.remove();
            removedEventBlocks.push(id);
        });

        //init data for edit
        $(".editEventBlock").off("click").on("click", function () {
            var $parent = $(this).parent();
            var id = $parent.attr("id").split("-")[1];
            var eventBlock = eventBlocks[id];
            $('#editEventBlockId').data("id", id);
            $("#editEventBlockTitle").val(eventBlock.title);
            $("#editEventBlockShortName").val(eventBlock.short_name);
            $("#editEventBlockShortNameEn").val(eventBlock.short_name_en);
            $("#editEventBlockImage").val(eventBlock.image);
            $("#editEventBlockContent").val(eventBlock.content);
            $("#editEventBlockContentEn").val(eventBlock.content_en);
        });

        //save edited
        $("#saveEditEventBlock").off('click').on('click', function () {
            var id = parseInt($('#editEventBlockId').data("id"));
            var eventBlock = eventBlocks[id];
            eventBlock.title = $("#editEventBlockTitle").val();
            eventBlock.short_name = $("#editEventBlockShortName").val();
            eventBlock.short_name_en = $("#editEventBlockShortNameEn").val();
            eventBlock.image = $("#editEventBlockImage").val();
            eventBlock.content = $("#editEventBlockContent").val();
            eventBlock.content_en = $("#editEventBlockContentEn").val();

            $('#removeEventBlock-' + id + ' > label').text(eventBlock.short_name);
        });
    }

    function addVoucherGroupItem(voucherGroup) {
        //add element to show
        $("#voucherGroup").append("<div id=\"removeVoucherGroup-" + voucherGroup.voucher_group_id + "\" class=\"checkbox\">\n" +
            "        <label class=\"col-lg-6\">\n" + voucherGroup.voucher_group_name +
            "        </label>\n" +
            "        <button type=\"button\" class=\"btn btn-danger btn-xs removeVoucherGroup\">\n" +
            "        <i class=\"fa fa-minus\"></i>\n" +
            "        </button>\n" +
            "        <button type=\"button\" class=\"btn btn-warning btn-xs editVoucherGroup\" data-toggle=\"modal\" data-target=\"#editVoucherGroups\">\n" +
            "        <i class=\"fa fa-edit\"></i>\n" +
            "        </button>\n" +
            "        </div>");

        countVoucherGroup++;

        //reset value for next add
        $("#addVoucherGroupType").val('1');
        $("#addVoucherGroupUseCode").prop('checked', false).change();
        $("#addVoucherGroupUseCondition").prop('checked', true).change();
        $("#addVoucherGroupName").val('') ;
        $("#addVoucherGroupNameEn").val('') ;
        $("#addVoucherGroupNote").val('');
        $("#addVoucherGroupNoteEn").val('');
        $("#nTicketToBuy").val('');
        $("#nPercentDiscount").val('');
        $("#nTicketToget").val('');
        $("#addVoucherGroupSTime").data("DateTimePicker").clear();
        $("#addVoucherGroupETime").data("DateTimePicker").clear();

        //have to put removed id into array, then re-process when submit
        //remove item shown
        $(".removeVoucherGroup").off("click").on("click", function () {
            var $parent = $(this).parent();
            var id = $parent.attr("id").split("-")[1];
            $parent.remove();
            removedVoucherGroups.push(id);
        });

        //init data for edit
        $(".editVoucherGroup").off("click").on("click", function () {
            var $parent = $(this).parent();
            var id = $parent.attr("id").split("-")[1];
            var voucher_group = voucher_groups[id];
            $('#editVoucherGroupId').data("id", id);
            $('#editVoucherGroupId').html(id.indexOf('_') >= 0 ? '' : ('Event Group Id: ' + id));
            $('#editVoucherGroupUseCode').prop('checked', voucher_group.use_code).change();
            $('#editVoucherGroupUseCondition').prop('checked', voucher_group.use_condition).change();
            $('#editVoucherGroupType').val(voucher_group.voucher_group_type);
            $('#editVoucherGroupName').val(voucher_group.voucher_group_name);
            $('#editVoucherGroupNameEn').val(voucher_group.voucher_group_name_en);
            $("#editVoucherGroupNote").val(voucher_group.voucher_group_note);
            $("#editVoucherGroupNoteEn").val(voucher_group.voucher_group_note_en);
            $("#editVoucherGroupSTime").data("DateTimePicker").defaultDate(new Date(voucher_group.start_time));
            $("#editVoucherGroupETime").data("DateTimePicker").defaultDate(new Date(voucher_group.end_time));
            $("#editnTicketToBuy").val(voucher_group.n_ticket_tobuy);
            $("#editticketIdTobuy").val(voucher_group.ticket_id_tobuy);
            $("#editnPercentDiscount").val(voucher_group.n_percent_discount);
            $("#editnTicketToget").val(voucher_group.n_ticket_toget);
            $("#editticketIdToget").val(voucher_group.ticket_id_toget);
        });

        //save edited
        $("#saveEditVoucherGroup").off('click').on('click', function () {
            var id = parseInt($('#editVoucherGroupId').data("id"));
            var voucher_group = voucher_groups[id];
            voucher_group.voucher_group_name = $('#editVoucherGroupName').val();
            voucher_group.use_code = $('#editVoucherGroupUseCode').prop('checked') ? 1 : 0;
            voucher_group.use_condition = $('#editVoucherGroupUseCondition').prop('checked') ? 1 : 0;
            voucher_group.voucher_group_name_en = $('#editVoucherGroupNameEn').val();
            voucher_group.voucher_group_type = $('#editVoucherGroupType').val();
            voucher_group.event_id = event_id;
            voucher_group.voucher_group_note = $('#editVoucherGroupNote').val();
            voucher_group.voucher_group_note_en = $('#editVoucherGroupNoteEn').val();
            voucher_group.n_ticket_tobuy = parseInt($('#editnTicketToBuy').val());
            voucher_group.ticket_id_tobuy = parseInt($('#editticketIdTobuy').val());
            voucher_group.n_percent_discount = parseInt($('#editnPercentDiscount').val());
            voucher_group.n_ticket_toget = parseInt($('#editnTicketToget').val());
            voucher_group.ticket_id_toget = parseInt($('#editticketIdToget').val());
            voucher_group.start_time = new Date($('#editVoucherGroupSTime').data('date')).getTime();
            voucher_group.end_time = new Date($('#editVoucherGroupETime').data('date')).getTime();

            $("#editVoucherGroupSTime").data("DateTimePicker").clear();
            $("#editVoucherGroupETime").data("DateTimePicker").clear();

            $('#removeVoucherGroup-' + id + ' > label').text(voucher_group.voucher_group_name);
        });
    }

    function addBannerItem(banner) {
        banners[countBanners] = banner;
        //add element to show
        $("#banner").append("<div id=\"removeEventBanner-" + countBanners + "\" class=\"checkbox\">\n" +
            "        <label  class=\"col-lg-6\">\n" + banner +
            "        </label>\n" +
            "        <button type=\"button\" class=\"btn btn-danger btn-xs removeEventBanner\">\n" +
            "        <i class=\"fa fa-minus\"></i>\n" +
            "        </button>\n" +
            "        <button type=\"button\" class=\"btn btn-warning btn-xs editEventBanners\" data-toggle=\"modal\" data-target=\"#editEventBanners\">\n" +
            "        <i class=\"fa fa-edit\"></i>\n" +
            "        </button>\n" +
            "        </div>");

        countBanners++;

        //reset value for next add
        $("#addEventBannerUrl").val('') ;

        //have to put removed id into array, then re-process when submit
        //remove item shown
        $(".removeEventBanner").off("click").on("click", function () {
            var $parent = $(this).parent();
            var id = parseInt($parent.attr("id").split("-")[1]);
            $parent.remove();
            removedBanners.push(id);
        });

        //init data for edit
        $(".editEventBanners").off("click").on("click", function () {
            var $parent = $(this).parent();
            var id = parseInt($parent.attr("id").split("-")[1]);
            $('#editEventBannerId').data("id", id);
            $("#editEventBannerUrl").val(banners[id]) ;
        });

        //save edited
        $("#saveEditEventBanner").off('click').on('click', function () {
            var id = parseInt($('#editEventBannerId').data("id"));
            banners[id] = $("#editEventBannerUrl").val();
            $('#removeEventBanner-' + id + ' > label').text(banners[id]);
        });
    }

    for(var i = 0 ; i < jsonEventBlocks.length; i++){
        eventBlocks[countEventBlock] = jsonEventBlocks[i];

        addEventBlockItem(jsonEventBlocks[i]);
    }

    if(jsonTickets) {
        for (var i = 0; i < jsonTickets.length; i++) {
            tickets[jsonTickets[i].ticket_id] = jsonTickets[i];

            setSelect('ticketIdTobuy', jsonTickets[i].ticket_id, jsonTickets[i].name);
            setSelect('ticketIdToget', jsonTickets[i].ticket_id, jsonTickets[i].name);
            setSelect('editticketIdTobuy', jsonTickets[i].ticket_id, jsonTickets[i].name);
            setSelect('editticketIdToget', jsonTickets[i].ticket_id, jsonTickets[i].name);

            addTicketItem(jsonTickets[i]);
        }
    }

    for(var i = 0 ; i < jsonVoucherGroups.length; i++){
        jsonVoucherGroups[i].event_id = event_id;
        voucher_groups[jsonVoucherGroups[i].voucher_group_id] = jsonVoucherGroups[i];
        addVoucherGroupItem(jsonVoucherGroups[i]);
    }

    for(var i = 0 ; i < jsonBanners.length; i++){
        addBannerItem(jsonBanners[countBanners]);
    }

    $("#addEventBanner").click(function () {
        banners[countBanners] = $('#addEventBannerUrl').val();
        addBannerItem(banners[countBanners])
    });

    $("#addVoucherGroup").click(function (){
        var voucher_group = {};

        voucher_group.voucher_group_id = countVoucherGroup + '_';
        voucher_group.voucher_group_name = $('#addVoucherGroupName').val();
        voucher_group.voucher_group_name_en = $('#addVoucherGroupNameEn').val();
        voucher_group.event_id = event_id;
        voucher_group.voucher_group_type = $('#addVoucherGroupType').val();
        voucher_group.voucher_group_note = $('#addVoucherGroupNote').val();
        voucher_group.voucher_group_note_en = $('#addVoucherGroupNoteEn').val();
        voucher_group.n_ticket_tobuy = parseInt($('#nTicketToBuy').val());
        voucher_group.ticket_id_tobuy = parseInt($('#ticketIdTobuy').val());
        voucher_group.n_percent_discount = parseInt($('#nPercentDiscount').val());
        voucher_group.n_ticket_toget = parseInt($('#nTicketToget').val());
        voucher_group.ticket_id_toget = parseInt($('#ticketIdToget').val());
        voucher_group.start_time = new Date($('#addVoucherGroupSTime').data('date')).getTime();
        voucher_group.end_time = new Date($('#addVoucherGroupETime').data('date')).getTime();

        voucher_groups[countVoucherGroup + '_'] = voucher_group;

        addVoucherGroupItem(voucher_group);
    });

    $("#addEventBlock").click(function () {
        var eventBlock = {};
        eventBlock.title = $("#addEventBlockTitle").val();
        eventBlock.short_name = $("#addEventBlockShortName").val();
        eventBlock.short_name_en = $("#addEventBlockShortNameEn").val();
        eventBlock.image = $("#addEventBlockImage").val();
        eventBlock.content = $("#addEventBlockContent").val();
        eventBlock.content_en = $("#addEventBlockContentEn").val();
        eventBlocks[countEventBlock] = eventBlock;

        addEventBlockItem(eventBlock);
    });

    $("#addTicket").click(function () {
        var ticket = {};
        ticket.enable = $("#addTicketStatus").prop('checked') ? 1 : 0;
        ticket.no_COD = $("#addTicketNoCOD").prop('checked') ? 1 : 0;
        ticket.no_promotion = $("#addTicketNoPromotion").prop('checked') ? 1 : 0;
        ticket.ticket_id = -1;
        ticket.event_id = -1;
        ticket.name = $("#addTicketName").val();
        ticket.display_name = $("#addTicketDisplayName").val();
        ticket.display_name_en = $("#addTicketDisplayNameEn").val();
        ticket.price = parseInt($("#addTicketPrice").val());
        ticket.min_buy = parseInt($("#addTicketMinbuy").val());
        ticket.max_buy = parseInt($("#addTicketMaxbuy").val());
        ticket.quantity = parseInt($("#addTicketQuantity").val());
        ticket.start_time = new Date($("#addTicketSTime").data('date')).getTime();
        ticket.end_time = new Date($("#addTicketETime").data('date')).getTime();
        ticket.note = $("#addTicketNote").val();
        ticket.note_en = $("#addTicketNoteEn").val();
        tickets[countTicket + '_'] = ticket;

        addTicketItem(ticket);
    });

    $('#submit').click(function () {
        var fEventBlocks = [], fTickets = [], fVoucherGroups = [], fBanners = [];

        for(var i = 0 ; i < eventBlocks.length; i++){
            if(removedEventBlocks.indexOf(i) < 0){
                fEventBlocks.push(eventBlocks[i]);
            }
        }

        for(var i = 0 ; i < banners.length; i++){
            if(removedBanners.indexOf(i) < 0){
                fBanners.push(banners[i]);
            }
        }

        for(id in tickets){
            if(removedTickets.indexOf(id) < 0){
                tickets[id].ticket_id = tickets[id].ticket_id.toString().indexOf('_') !== -1 ? -1 : parseInt(tickets[id].ticket_id);
                fTickets.push(tickets[id]);
            }
        }

        for(id in voucher_groups){
            if(removedVoucherGroups.indexOf(id) < 0){
                voucher_groups[id].voucher_group_id = voucher_groups[id].voucher_group_id.toString().indexOf('_') !== -1 ? -1 : parseInt(voucher_groups[id].voucher_group_id);
                fVoucherGroups.push(voucher_groups[id]);
            }
        }

        event.event_id = $("#jsonId").data('data');
        event.is_dual_language = $("#eventDualLanguage").prop('checked') ? 1 : 0;
        event.name = $("#eventName").val();
        event.url = $("#eventUrl").val();
        event.bigtime_url = $("#eventBigtimeUrl").val();
        event.logo = $("#eventLogo").val();
        event.banner = JSON.stringify(fBanners);
        event.intro = $("#eventIntro").val();
        event.instagram = $("#eventInstagram").val();
        event.start_time = $('#eventSTime').data('date') ? new Date($('#eventSTime').data('date')).getTime() : 0;
        event.end_time = $('#eventETime').data('date') ? new Date($('#eventETime').data('date')).getTime() : 0;
        event.display_time = $('#eventDTime').val();
        event.description = $("#eventDescription").val();
        event.description_en = $("#eventDescriptionEn").val();
        event.location = $("#eventLocation").val();
        event.address = $("#eventAddress").val();
        event.address_en = $("#eventAddressEn").val();
        event.sponsor_name = $("#sponsorName").val();
        event.sponsor_image = $("#sponsorImage").val();
        event.event_blocks = JSON.stringify(fEventBlocks);
        event.tickets = JSON.stringify(fTickets);
        event.voucher_groups = JSON.stringify(fVoucherGroups);
        event.list_payment_method = [];
        for(var i = 2; i < 6; i++){
            if($("#eventPaymentMethod_" + i).prop('checked')){
                event.list_payment_method.push(i);
            }
        }

        if($("#eventPaymentMethod_" + 7).prop('checked')){
            event.list_payment_method.push(7);
        }

        event.list_payment_method = event.list_payment_method.join(",");

        console.log(event);

        $.ajax({
            type: "POST",
            url: "/event/update",
            data: JSON.stringify(event),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data){
                if(data > 0){
                    $(".alert-success").fadeTo(2000, 500).slideUp(500, function(){
                        location.reload();
                    });
                }
            },
            failure: function(errMsg) {
                alert(errMsg);
            }
        });
    });
});
