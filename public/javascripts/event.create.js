$( document ).ready(function() {
    var event = {},
        countEventBlock = 0, countTicket = 0, countBanner = 0,
        eventBlocks = [], tickets = [], banners = [],
        removedEventBlocks = [], removedTickets = [], removedBanners = [];

    function setTime(start_item, end_item) {
        $(start_item).datetimepicker();
        $(end_item).datetimepicker({
            useCurrent: false //Important! See issue #1075
        });

        $(start_item).on("dp.change", function (e) {
            $(end_item).data("DateTimePicker").minDate(e.date);
            //$(start_item).data("DateTimePicker").hide();
            //$(end_item).data("DateTimePicker").show();
        });

        $(end_item).on("dp.change", function (e) {
            //$(end_item).data("DateTimePicker").hide();
        });
    }

    setTime('#eventSTime', '#eventETime');
    setTime('#addTicketSTime', '#addTicketETime');
    setTime('#editTicketSTime', '#editTicketETime');

    $("#addEventBanner").click(function () {
        banners[countBanner] = $('#addEventBannerUrl').val();
        //add element to show
        $("#banner").append("<div id=\"removeEventBanner-" + countBanner + "\" class=\"checkbox\">\n" +
            "        <label  class=\"col-lg-6\">\n" + banners[countBanner] +
            "        </label>\n" +
            "        <button type=\"button\" class=\"btn btn-danger btn-xs removeEventBanner\">\n" +
            "        <i class=\"fa fa-minus\"></i>\n" +
            "        </button>\n" +
            "        <button type=\"button\" class=\"btn btn-warning btn-xs editEventBanners\" data-toggle=\"modal\" data-target=\"#editEventBanners\">\n" +
            "        <i class=\"fa fa-edit\"></i>\n" +
            "        </button>\n" +
            "        </div>");

        countBanner++;

        //reset value for next add
        $("#addEventBannerUrl").val('') ;

        //have to put removed id into array, then re-process when submit
        //remove item shown
        $(".removeEventBanner").off("click").on("click", function () {
            var $parent = $(this).parent();
            var id = parseInt($parent.attr("id").split("-")[1]);
            $parent.remove();
            removedBanners.push(id);
        });

        //init data for edit
        $(".editEventBanners").off("click").on("click", function () {
            var $parent = $(this).parent();
            var id = parseInt($parent.attr("id").split("-")[1]);
            $('#editEventBannerId').data("id", id);
            $("#editEventBannerUrl").val(banners[id]) ;
        });

        //save edited
        $("#saveEditEventBanner").off('click').on('click', function () {
            var id = parseInt($('#editEventBannerId').data("id"));
            banners[id] = $("#editEventBannerUrl").val();
            $('#removeEventBanner-' + id + ' > label').text(banners[id]);
        });
    });

    $("#addEventBlock").click(function () {
        var eventBlock = {};
        eventBlock.title = $("#addEventBlockTitle").val();
        eventBlock.short_name = $("#addEventBlockShortName").val();
        eventBlock.short_name_en = $("#addEventBlockShortNameEn").val();
        eventBlock.image = $("#addEventBlockImage").val();
        eventBlock.content = $("#addEventBlockContent").val();
        eventBlock.content_en = $("#addEventBlockContentEn").val();
        eventBlocks[countEventBlock] = eventBlock;


        //add element to show
        $("#eventBlock").append("<div id=\"removeEventBlock-" + countEventBlock + "\" class=\"checkbox\">\n" +
            "        <label class=\"col-lg-6\">\n" + eventBlock.short_name +
            "        </label>\n" +
            "        <button type=\"button\" class=\"btn btn-danger btn-xs removeEventBlock\">\n" +
            "        <i class=\"fa fa-minus\"></i>\n" +
            "        </button>\n" +
            "        <button type=\"button\" class=\"btn btn-warning btn-xs editEventBlock\" data-toggle=\"modal\" data-target=\"#editEventBlock\">\n" +
            "        <i class=\"fa fa-edit\"></i>\n" +
            "        </button>\n" +
            "        </div>");

        countEventBlock++;

        //reset value for next add
        $("#addEventBlockTitle").val('') ;
        $("#addEventBlockShortName").val('') ;
        $("#addEventBlockShortNameEn").val('') ;
        $("#addEventBlockImage").val('');
        $("#addEventBlockContent").val('');
        $("#addEventBlockContentEn").val('');

        //have to put removed id into array, then re-process when submit
        //remove item shown
        $(".removeEventBlock").off("click").on("click", function () {
            var $parent = $(this).parent();
            var id = parseInt($parent.attr("id").split("-")[1]);
            $parent.remove();
            removedEventBlocks.push(id);
        });

        //init data for edit
        $(".editEventBlock").off("click").on("click", function () {
            var $parent = $(this).parent();
            var id = parseInt($parent.attr("id").split("-")[1]);
            var eventBlock = eventBlocks[id];
            $('#editEventBlockId').data("id", id);
            $("#editEventBlockTitle").val(eventBlock.title) ;
            $("#editEventBlockShortName").val(eventBlock.short_name) ;
            $("#editEventBlockShortNameEn").val(eventBlock.short_name_en) ;
            $("#editEventBlockImage").val(eventBlock.image);
            $("#editEventBlockContent").val(eventBlock.content);
            $("#editEventBlockContentEn").val(eventBlock.content_en);
        });

        //save edited
        $("#saveEditEventBlock").off('click').on('click', function () {
            var id = parseInt($('#editEventBlockId').data("id"));
            var eventBlock = eventBlocks[id];
            eventBlock.title = $("#editEventBlockTitle").val();
            eventBlock.short_name = $("#editEventBlockShortName").val();
            eventBlock.short_name_en = $("#editEventBlockShortNameEn").val();
            eventBlock.image = $("#editEventBlockImage").val();
            eventBlock.content = $("#editEventBlockContent").val();
            eventBlock.content_en = $("#editEventBlockContentEn").val();

            $('#removeEventBlock-' + id + ' > label').text(eventBlock.short_name);
        });
    });

    $("#addTicket").click(function () {
        var ticket = {};
        ticket.enable = $("#addTicketStatus").prop('checked') ? 1 : 0;
        ticket.no_COD = $("#addTicketNoCOD").prop('checked') ? 1 : 0;
        ticket.no_promotion = $("#addTicketNoPromotion").prop('checked') ? 1 : 0;
        ticket.ticket_id = -1;
        ticket.event_id = -1;
        ticket.name = $("#addTicketName").val();
        ticket.display_name = $("#addTicketDisplayName").val();
        ticket.display_name_en = $("#addTicketDisplayNameEn").val();
        ticket.price = parseInt($("#addTicketPrice").val());
        ticket.min_buy = parseInt($("#addTicketMinbuy").val());
        ticket.max_buy = parseInt($("#addTicketMaxbuy").val());
        ticket.quantity = parseInt($("#addTicketQuantity").val());
        ticket.start_time = new Date($("#addTicketSTime").data('date')).getTime();
        ticket.end_time = new Date($("#addTicketETime").data('date')).getTime();
        ticket.note = $("#addTicketNote").val();
        ticket.note_en = $("#addTicketNoteEn").val();
        tickets[countTicket] = ticket;

        $("#ticket").append("<div id=\"removeTicket-" + countTicket + "\" class=\"checkbox\">\n" +
            "        <label class=\"col-lg-6\">\n" + ticket.name +
            "        </label>\n" +
            "        <button type=\"button\" class=\"btn btn-danger btn-xs removeTicket\">\n" +
            "        <i class=\"fa fa-minus\"></i>\n" +
            "        </button>\n" +
            "        <button type=\"button\" class=\"btn btn-warning btn-xs editTicket\" data-toggle=\"modal\" data-target=\"#editTicket\">\n" +
            "        <i class=\"fa fa-edit\"></i>\n" +
            "        </button>\n" +
            "        </div>")


        countTicket++;

        //reset value for next add
        $("#addTicketStatus").prop('checked', true).change();
        $("#addTicketNoCOD").prop('checked', false).change();
        $("#addTicketNoPromotion").prop('checked', false).change();
        $("#addTicketName").val('');
        $("#addTicketDisplayName").val('');
        $("#addTicketDisplayName_en").val('');
        $("#addTicketPrice").val('');
        $("#addTicketMinbuy").val('');
        $("#addTicketMaxbuy").val('');
        $("#addTicketQuantity").val('');
        $("#addTicketSTime").data("DateTimePicker").clear();
        $("#addTicketETime").data("DateTimePicker").clear();
        $("#addTicketNote").val('');
        $("#addTicketNoteEn").val('');

        //have to put removed id into array, then re-process when submit
        //remove item shown
        $(".removeTicket").off("click").on("click", function () {
            var $parent = $(this).parent();
            var id = parseInt($parent.attr("id").split("-")[1]);
            $parent.remove();
            removedTickets.push(id);
        });

        //init data for edit
        $(".editTicket").off("click").on("click", function () {
            var $parent = $(this).parent();
            var id = parseInt($parent.attr("id").split("-")[1]);
            var ticket = tickets[id];
            $("#editTicketStatus").prop('checked', ticket.status).change();
            $("#editTicketNoCOD").prop('checked', ticket.no_COD).change();
            $("#editTicketNoPromotion").prop('checked', ticket.no_promotion).change();
            $('#editTicketId').val(ticket.ticket_id);
            $("#editTicketName").val(ticket.name);
            $("#editTicketDisplayName").val(ticket.display_name);
            $("#editTicketDisplayNameEn").val(ticket.display_name_en);
            $("#editTicketPrice").val(ticket.price);
            $("#editTicketMinbuy").val(ticket.min_buy);
            $("#editTicketMaxbuy").val(ticket.max_buy);
            $("#editTicketQuantity").val(ticket.quantity);
            $("#editTicketSTime").data("DateTimePicker").defaultDate(new Date(ticket.start_time));
            $("#editTicketETime").data("DateTimePicker").defaultDate(new Date(ticket.end_time));
            $("#editTicketNote").val(ticket.note);
            $("#editTicketNoteEn").val(ticket.note_en);
        });

        //save edited
        $("#saveEditTicket").off('click').on('click', function () {
            var id = parseInt($('#editTicketId').data("id"));
            var ticket = tickets[id];
            ticket.ticket_id = -1;
            ticket.event_id = -1;
            ticket.enable = $("#editTicketStatus").prop('checked') ? 1 : 0;
            ticket.no_COD = $("#editTicketNoCOD").prop('checked') ? 1 : 0;
            ticket.no_promotion = $("#editTicketNoPromotion").prop('checked') ? 1 : 0;
            ticket.name = $("#editTicketName").val();
            ticket.display_name = $("#editTicketDisplayName").val();
            ticket.display_name_en = $("#editTicketDisplayNameEn").val();
            ticket.price = parseInt($("#editTicketPrice").val());
            ticket.min_buy = parseInt($("#editTicketMinbuy").val());
            ticket.max_buy = parseInt($("#editTicketMaxbuy").val());
            ticket.quantity = parseInt($("#editTicketQuantity").val());
            ticket.start_time = $("#editTicketSTime").data('date');
            ticket.end_time = $("#editTicketETime").data('date');
            ticket.note = $("#editTicketNote").val();
            ticket.note_en = $("#editTicketNoteEn").val();

            $('#removeTicket-' + id + ' > label').text(ticket.name);
        });
    });

    $('#submit').click(function () {
        var fEventBlocks = [], fTickets = [], fBanners = [];

        for(var i = 0 ; i < eventBlocks.length; i++){
            if(removedEventBlocks.indexOf(i) < 0){
                fEventBlocks.push(eventBlocks[i]);
            }
        }

        for(var i = 0 ; i < tickets.length; i++){
            if(removedTickets.indexOf(i) < 0){
                fTickets.push(tickets[i]);
            }
        }

        for(var i = 0 ; i < banners.length; i++){
            if(removedBanners.indexOf(i) < 0){
                fBanners.push(banners[i]);
            }
        }

        event.event_id = -1;
        event.is_dual_language = $("#eventDualLanguage").prop('checked') ? 1 : 0;
        event.name = $("#eventName").val();
        event.url = $("#eventUrl").val();
        event.bigtime_url = $("#eventBigtimeUrl").val();
        event.logo = $("#eventLogo").val();
        event.banner = JSON.stringify(fBanners);
        event.intro = $("#eventIntro").val();
        event.instagram = $("#eventInstagram").val();
        event.description = $("#eventDescription").val();
        event.description_en = $("#eventDescriptionEn").val();
        event.location = $("#eventLocation").val();
        event.address = $("#eventAddress").val();
        event.address_en = $("#eventAddressEn").val();
        event.sponsor_name = $("#sponsorName").val();
        event.sponsor_image = $("#sponsorImage").val();
        event.start_time = $('#eventSTime').data('date') ? new Date($('#eventSTime').data('date')).getTime() : 0;
        event.end_time = $('#eventETime').data('date') ? new Date($('#eventETime').data('date')).getTime() : 0;
        event.display_time = $("#eventDTime").val();
        event.event_blocks = JSON.stringify(fEventBlocks);
        event.tickets = JSON.stringify(fTickets);
        event.promotions = "";
        event.list_payment_method = [];
        for(var i = 2; i < 6; i++){
            if($("#eventPaymentMethod_" + i).prop('checked')){
                event.list_payment_method.push(i);
            }
        }

        if($("#eventPaymentMethod_" + 7).prop('checked')){
            event.list_payment_method.push(7);
        }
        event.list_payment_method = event.list_payment_method.join(",");

        console.log(event);

        $.ajax({
            type: "POST",
            url: "/event/add",
            data: JSON.stringify(event),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data){
                if(data > 0){
                    $(".alert-success").fadeTo(2000, 500).slideUp(500, function(){
                        location.reload();
                        //$(".alert-success").slideUp(500);
                    });
                }
                //notice: success
                //reset form data
            },
            failure: function(errMsg) {
                alert(errMsg);
            }
        });
    });
});
