name := "ICreateEvent"
 
version := "1.0" 
      
lazy val `icreateevent` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
      
resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"

scalaVersion := "2.12.2"

libraryDependencies ++= Seq(jdbc , ehcache , ws , specs2 % Test , guice, jdbc, "com.typesafe.play" %% "anorm" % "2.5.3" )

libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.41"

libraryDependencies += "com.google.api-client" % "google-api-client" % "1.22.0"
libraryDependencies += "com.google.oauth-client" % "google-oauth-client" % "1.22.0"
libraryDependencies += "com.google.apis" % "google-api-services-drive" % "v3-rev107-1.22.0"
libraryDependencies += "com.google.code.gson" % "gson" % "2.2.4"
libraryDependencies += "com.googlecode.json-simple" % "json-simple" % "1.1"




unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  

      