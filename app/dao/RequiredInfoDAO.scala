package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import models.RequiredInfo
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

class RequiredInfoDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
  val parsedRequiredInfo = {
        get[Long]("id") ~
        get[String]("name") ~
        get[String]("place_holder") map {
      case id ~ name ~ place_holder
      => RequiredInfo(id, name, place_holder)
    }
  }
}
