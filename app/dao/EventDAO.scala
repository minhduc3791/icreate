package dao

import java.util
import javax.inject.Inject

import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}
import anorm._
import models.{Event, EventBlock, RequiredInfo, Ticket}
import anorm.SqlParser._

class EventDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
    val parsedEvent = {
        get[Long]("event_id") ~
            get[Int]("is_dual_language") ~
            get[String]("url") ~
            get[String]("bigtime_url") ~
            get[String]("name") ~
            get[Long]("start_time") ~
            get[Long]("end_time") ~
            get[String]("display_time") ~
            get[String]("logo") ~
            get[String]("banner") ~
            get[String]("intro") ~
            get[String]("instagram") ~
            get[String]("description") ~
            get[String]("description_en") ~
            get[String]("event_blocks") ~
            get[String]("tickets") ~
            get[String]("voucher_groups") ~
            get[String]("address") ~
            get[String]("address_en") ~
            get[String]("location") ~
            get[String]("sponsor_name") ~
            get[String]("sponsor_image") ~
            get[String]("list_payment_method") map {
            case id ~ is_dual_language ~ url ~ bigtime_url ~ name ~ start_time ~ end_time ~ display_time ~ logo ~ banner ~ intro ~ instagram ~ description ~ description_en ~ event_blocks ~ tickets ~ voucher_groups ~ address ~ address_en ~ location ~ sponsor_name ~ sponsor_image ~list_payment_method
            => Event(id, is_dual_language, url, bigtime_url, name, start_time, end_time, display_time, logo, banner, intro, instagram, description, description_en, event_blocks, tickets, voucher_groups, address, address_en, location, sponsor_name, sponsor_image, list_payment_method)
        }
    }

    def add(event: Event): Long = {
        var result: Long = -1
        db.withConnection { implicit connection =>
            val id: Option[Long] =
                SQL("INSERT into event(url, bigtime_url, is_dual_language, name, start_time, end_time, display_time, logo, banner, intro, instagram, description, description_en, event_blocks, tickets, voucher_groups, address, address_en, location, sponsor_name, sponsor_image, list_payment_method) " +
                    "values ({url}, {bigtime_url}, {is_dual_language}, {name}, {start_time}, {end_time}, {display_time}, {logo}, {banner}, {intro}, {instagram}, {description}, {description_en}, {event_blocks}, {tickets}, {voucher_groups}, {address}, {address_en}, {location}, {sponsor_name}, {sponsor_image}, {list_payment_method})")
                    .on("url" -> event.url).on("bigtime_url" -> event.bigtime_url).on("name" -> event.name).on("start_time" -> event.start_time).on("end_time" -> event.end_time).on("display_time" -> event.display_time).on("logo" -> event.logo)
                    .on("banner" -> event.banner).on("intro" -> event.intro).on("instagram" -> event.instagram).on("description" -> event.description).on("description_en" -> event.description_en).on("event_blocks" -> event.event_blocks)
                    .on("tickets" -> event.tickets).on("voucher_groups" -> event.voucher_groups).on("address" -> event.address).on("address_en" -> event.address_en).on("location" -> event.location)
                    .on("sponsor_name" -> event.sponsor_name).on("sponsor_image" -> event.sponsor_image).on("is_dual_language" -> event.is_dual_language).on("list_payment_method" -> event.list_payment_method)
                    .executeInsert()

            result = id match {
                case Some(number) => number
                case None => -1
            }
        }

        result
    }

    def update(event: Event): Int = {
        db.withConnection { implicit connection =>
            SQL(
                """UPDATE event SET url={url}, bigtime_url={bigtime_url}, is_dual_language={is_dual_language}, name={name}, start_time={start_time}, end_time={end_time}, display_time={display_time}, logo={logo}, banner={banner}, intro={intro}, instagram={instagram},
                   description={description}, description_en={description_en}, event_blocks={event_blocks}, tickets={tickets}, voucher_groups={voucher_groups}, address={address}, address_en={address_en}, location={location}, sponsor_name={sponsor_name},
                   sponsor_image={sponsor_image}, list_payment_method={list_payment_method} WHERE event_id={event_id}""")
                .on("url" -> event.url).on("bigtime_url" -> event.bigtime_url).on("name" -> event.name).on("start_time" -> event.start_time).on("end_time" -> event.end_time).on("display_time" -> event.display_time).on("logo" -> event.logo)
                .on("banner" -> event.banner).on("intro" -> event.intro).on("instagram" -> event.instagram).on("description" -> event.description).on("description_en" -> event.description_en).on("event_blocks" -> event.event_blocks)
                .on("tickets" -> event.tickets).on("voucher_groups" -> event.voucher_groups).on("address" -> event.address).on("address_en" -> event.address_en).on("location" -> event.location)
                .on("sponsor_name" -> event.sponsor_name).on("sponsor_image" -> event.sponsor_image).on("event_id" -> event.event_id).on("is_dual_language" -> event.is_dual_language).on("list_payment_method" -> event.list_payment_method)
                .executeUpdate()
        }
    }

    def getEventById(event_id: Long): Event = {
        db.withConnection { implicit connection =>
            val parser: RowParser[Event] = Macro.namedParser[Event]
            val event = SQL("SELECT * FROM event where event_id = {event_id}")
                .on("event_id" -> event_id)
                .as(parser.*)
            event(0)
        }
    }

    def getEventByName(id: Long): Long = {
        1L
    }

    def getEventByUrl(url: String): Event = {
        var res: Event = null
        db.withConnection { implicit connection =>
            val parser: RowParser[Event] = Macro.namedParser[Event]
            val event = SQL("SELECT * FROM event where url = {url}")
                .on("url" -> url)
                .as(parser.*)
            if (event != Nil) res = event(0)
        }

        res
    }

    def getEvents(offset: Long, limit: Long): List[Event] = {
        db.withConnection { implicit connection =>
            val parser: RowParser[Event] = Macro.namedParser[Event]
            val event = SQL("SELECT * FROM event LIMIT {limit} OFFSET {offset}")
                .on("limit" -> limit).on("offset" -> offset)
                .as(parsedEvent.*)

            event
        }
    }
}
