package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import models.Transaction
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

class TransactionDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
  val parsedTransaction = {
        get[Long]("transaction_id") ~
        get[String]("invoice_id") ~
        get[Int]("payment_method") ~
        get[Long]("create_time") ~
        get[Long]("finish_time") ~
        get[Int]("transaction_status") ~
        get[String]("username") ~
        get[String]("email") ~
        get[String]("phonenumber") ~
        get[String]("tickets_info") ~
        get[String]("seats_info") ~
        get[Long]("revenue_before") ~
        get[Long]("revenue_discount") ~
        get[Long]("revenue_after") ~
        get[String]("voucher_code") ~
        get[Long]("event_id") ~
        get[String]("event_name") ~
        get[Int]("ticket_status") ~
        get[String]("note") ~
        get[String]("address") map {
      case transaction_id ~ invoice_id ~ payment_method ~ create_time ~ finish_time ~ transaction_status ~ username ~ email ~ phonenumber ~ tickets_info
                ~ seats_info ~ revenue_before ~ revenue_discount ~ revenue_after ~ voucher_code ~ event_id ~ event_name ~ ticket_status ~ note ~ address
      => Transaction(transaction_id, invoice_id, payment_method, create_time, finish_time, transaction_status, username, email, phonenumber, tickets_info,
        seats_info, revenue_before, revenue_discount, revenue_after, voucher_code, event_id, event_name, ticket_status, note, address)
    }
  }

  def add(transaction: Transaction): Long = {
    var res = -1L
    db.withConnection { implicit connection =>
      val x = SQL("""INSERT into Transaction(invoice_id, payment_method, create_time, finish_time, transaction_status, username, email, phonenumber, tickets_info, seats_info,
        revenue_before, revenue_discount, revenue_after, voucher_code, event_id, event_name, ticket_status, note, address)
        values ({invoice_id}, {payment_method}, {create_time}, {finish_time}, {transaction_status}, {username}, {email}, {phonenumber}, {tickets_info}, {seats_info},
        {revenue_before}, {revenue_discount}, {revenue_after}, {voucher_code}, {event_id}, {event_name}, {ticket_status}, {note}, {address})""")
        .on("invoice_id" -> transaction.invoice_id).on("payment_method" -> transaction.payment_method).on("create_time" -> transaction.create_time).on("finish_time" -> transaction.finish_time)
        .on("transaction_status" -> transaction.transaction_status).on("username" -> transaction.username).on("email" -> transaction.email).on("phonenumber" -> transaction.phonenumber).on("tickets_info" -> transaction.tickets_info)
        .on("seats_info" -> transaction.seats_info).on("revenue_before" -> transaction.revenue_before).on("revenue_discount" -> transaction.revenue_discount)
        .on("revenue_after" -> transaction.revenue_after).on("voucher_code" -> transaction.voucher_code).on("event_id" -> transaction.event_id).on("event_name" -> transaction.event_name)
        .on("ticket_status" -> transaction.ticket_status).on("note" -> transaction.note).on("address" -> transaction.address)


      val id = x.executeInsert()
      res = id match {
          case Some(n) => 1
          case _ => -1
      }
    }
    res
  }

  def update(transaction: Transaction) : Int = {
    db.withConnection { implicit connection =>
      SQL("""UPDATE transaction SET invoice_id={invoice_id}, payment_method={payment_method}, create_time={create_time}, finish_time={finish_time}, finish_time={finish_time}, transaction_status={transaction_status},
           username={username}, phonenumber={phonenumber}, tickets_info={tickets_info}, seats_info={seats_info}, revenue_before={revenue_before}, revenue_discount={revenue_discount},
           revenue_after={revenue_after}, voucher_code={voucher_code}, event_id={event_id}, event_name={event_name}, ticket_status={ticket_status}, note={note}, address={address} WHERE id={id}""")
        .on("invoice_id" -> transaction.invoice_id).on("payment_method" -> transaction.payment_method).on("create_time" -> transaction.create_time).on("finish_time" -> transaction.finish_time)
        .on("transaction_status" -> transaction.transaction_status).on("username" -> transaction.username).on("phonenumber" -> transaction.phonenumber).on("tickets_info" -> transaction.tickets_info)
        .on("seats_info" -> transaction.seats_info).on("revenue_before" -> transaction.revenue_before).on("revenue_discount" -> transaction.revenue_discount)
        .on("revenue_after" -> transaction.revenue_after).on("voucher_code" -> transaction.voucher_code).on("event_id" -> transaction.event_id).on("event_name" -> transaction.event_name)
        .on("ticket_status" -> transaction.ticket_status).on("note" -> transaction.note).on("address" -> transaction.address)
        .executeUpdate()
    }
  }

  def getTransactionById(transaction_id: Long) : Transaction = {
    db.withConnection { implicit connection =>
      val parser: RowParser[Transaction] = Macro.namedParser[Transaction]
      val transaction = SQL("SELECT * FROM transaction where transaction_id = {transaction_id}")
        .on("transaction_id" -> transaction_id)
        .as(parser.*)
      transaction(0)
    }
  }

  def getTransactions(offset: Long, limit: Long): List[Transaction] = {
    db.withConnection { implicit connection =>
      val parser: RowParser[Transaction] = Macro.namedParser[Transaction]
      val event = SQL("SELECT * FROM transaction LIMIT {limit} OFFSET {offset}")
        .on("limit" -> limit).on("offset" -> offset)
        .as(parsedTransaction.*)

      event
    }
  }
}
