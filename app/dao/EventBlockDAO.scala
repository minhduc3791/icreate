package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import models.EventBlock
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

class EventBlockDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
  val parsedEventBlock = {
        get[Long]("id") ~
        get[String]("short_name") ~
        get[String]("short_name_en") ~
        get[String]("title") ~
        get[String]("image") ~
        get[String]("content") ~
        get[String]("content_en") map {
      case id ~ short_name ~ short_name_en ~ title ~ image ~ content ~ content_en
      => EventBlock(id, short_name, short_name_en, title, image, content, content_en)
    }
  }
}
