package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import models.VoucherGroup
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

class VoucherGroupDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
    val parsedVoucherGroup = {
        get[Long]("voucher_group_id") ~
            get[Long]("event_id") ~
            get[Int]("voucher_group_type") ~
            get[String]("voucher_group_name") ~
            get[String]("voucher_group_name_en") ~
            get[String]("voucher_group_note") ~
            get[String]("voucher_group_note_en") ~
            get[Int]("n_ticket_tobuy") ~
            get[Long]("ticket_id_tobuy") ~
            get[Double]("n_percent_discount") ~
            get[Int]("n_ticket_toget") ~
            get[Long]("ticket_id_toget") ~
            get[Long]("start_time") ~
            get[Long]("end_time") ~
            get[Int]("use_code") ~
            get[Int]("use_condition")map {
            case voucher_group_id ~ event_id ~ voucher_group_type ~ voucher_group_name ~ voucher_group_name_en ~ voucher_group_note ~ voucher_group_note_en ~ n_ticket_tobuy ~ ticket_id_tobuy ~ n_percent_discount ~ n_ticket_toget ~ ticket_id_toget ~ start_time ~ end_time ~ use_code ~ use_condition
            => VoucherGroup(voucher_group_id, event_id, voucher_group_type, voucher_group_name, voucher_group_name_en, voucher_group_note, voucher_group_note_en, n_ticket_tobuy, ticket_id_tobuy, n_percent_discount, n_ticket_toget, ticket_id_toget, start_time, end_time,  use_code, use_condition)
        }
    }

    def add(voucher_group: VoucherGroup): Long = {
        var result: Long = -1
        db.withConnection { implicit connection =>
            val id: Option[Long] =
                SQL("INSERT into event_voucher_group(voucher_group_name, voucher_group_name_en, event_id, voucher_group_type, voucher_group_note, voucher_group_note_en, n_ticket_tobuy, ticket_id_tobuy, n_percent_discount, n_ticket_toget, ticket_id_toget, start_time, end_time, use_code, use_condition) " +
                    "values ({voucher_group_name}, {voucher_group_name_en}, {event_id}, {voucher_group_type}, {voucher_group_note}, {voucher_group_note_en}, {n_ticket_tobuy}, {ticket_id_tobuy}, {n_percent_discount}, {n_ticket_toget}, {ticket_id_toget}, {start_time}, {end_time}, {use_code}, {use_condition})")
                    .on("voucher_group_name" -> voucher_group.voucher_group_name).on("event_id" -> voucher_group.event_id).on("voucher_group_note" -> voucher_group.voucher_group_note).on("n_ticket_tobuy" -> voucher_group.n_ticket_tobuy)
                    .on("ticket_id_tobuy" -> voucher_group.ticket_id_tobuy).on("n_percent_discount" -> voucher_group.n_percent_discount).on("n_ticket_toget" -> voucher_group.n_ticket_toget).on("ticket_id_toget" -> voucher_group.ticket_id_toget)
                    .on("start_time" -> voucher_group.start_time).on("end_time" -> voucher_group.end_time).on("voucher_group_name_en" -> voucher_group.voucher_group_name_en).on("voucher_group_type" -> voucher_group.voucher_group_type).on("voucher_group_note_en" -> voucher_group.voucher_group_note_en)
                    .on("use_code" -> voucher_group.use_code).on("use_condition" -> voucher_group.use_condition)
                    .executeInsert()

            result = id match {
                case Some(number) => number
                case None => -1
            }
        }

        result
    }

    def update(voucher_group: VoucherGroup): Int = {
        db.withConnection { implicit connection =>
            SQL(
                """UPDATE event_voucher_group SET voucher_group_name={voucher_group_name}, voucher_group_name_en={voucher_group_name_en}, event_id={event_id},  voucher_group_type={voucher_group_type}, voucher_group_note={voucher_group_note}, voucher_group_note_en={voucher_group_note_en}, n_ticket_tobuy={n_ticket_tobuy}, ticket_id_tobuy={ticket_id_tobuy}, n_percent_discount={n_percent_discount},
                n_ticket_toget={n_ticket_toget}, ticket_id_toget={ticket_id_toget}, start_time={start_time}, end_time={end_time}, use_code={use_code}, use_condition={use_condition}
                WHERE voucher_group_id={voucher_group_id}""")
                .on("voucher_group_name" -> voucher_group.voucher_group_name).on("event_id" -> voucher_group.event_id).on("voucher_group_note" -> voucher_group.voucher_group_note).on("n_ticket_tobuy" -> voucher_group.n_ticket_tobuy)
                .on("ticket_id_tobuy" -> voucher_group.ticket_id_tobuy).on("n_percent_discount" -> voucher_group.n_percent_discount).on("n_ticket_toget" -> voucher_group.n_ticket_toget).on("ticket_id_toget" -> voucher_group.ticket_id_toget)
                .on("start_time" -> voucher_group.start_time).on("end_time" -> voucher_group.end_time).on("voucher_group_id" -> voucher_group.voucher_group_id).on("voucher_group_name_en" -> voucher_group.voucher_group_name_en).on("voucher_group_type" -> voucher_group.voucher_group_type)
                .on("voucher_group_note_en" -> voucher_group.voucher_group_note_en).on("use_code" -> voucher_group.use_code).on("use_condition" -> voucher_group.use_condition)
                .executeUpdate()
        }
    }
}
