package dao

import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import models.{Event, EventBlock, RequiredInfo, Ticket}
import play.api.db.Database
import play.api.mvc.{BaseController, ControllerComponents}

class TicketDAO @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
    val parsedTicket = {
        get[Long]("ticket_id") ~
            get[Long]("event_id") ~
            get[String]("name") ~
            get[String]("display_name") ~
            get[String]("display_name_en") ~
            get[Long]("price") ~
            get[Long]("start_time") ~
            get[Long]("end_time") ~
            get[String]("note") ~
            get[String]("note_en") ~
            get[Int]("enable") ~
            get[Int]("min_buy") ~
            get[Int]("max_buy") ~
            get[Int]("count") ~
            get[Int]("quantity") ~
            get[Int]("no_COD") ~
            get[Int]("no_promotion") map {
            case ticket_id ~ event_id ~ name ~ display_name ~ display_name_en ~ price ~ start_time ~ end_time ~ note ~ note_en ~ enable ~ min_buy ~ max_buy ~ count ~ quantity ~ no_COD ~ no_promotion
            => Ticket(ticket_id, event_id, name, display_name, display_name_en, price, start_time, end_time, note, note_en, enable, min_buy, max_buy, count, quantity, no_COD, no_promotion)
        }
    }

    def add(ticket: Ticket): Long = {
        var result: Long = -1
        db.withConnection { implicit connection =>
            val id: Option[Long] =
                SQL("INSERT into Ticket(name, event_id, display_name, display_name_en, price, start_time, end_time, note, note_en, enable, min_buy, max_buy, count, quantity, no_COD, no_promotion) " +
                    "values ({name}, {event_id}, {display_name}, {display_name_en}, {price}, {start_time}, {end_time}, {note}, {note_en}, {enable}, {min_buy}, {max_buy} ,{count}, {quantity}, {no_COD}, {no_promotion})")
                    .on("name" -> ticket.name).on("event_id" -> ticket.event_id).on("display_name" -> ticket.display_name).on("display_name_en" -> ticket.display_name_en).on("price" -> ticket.price)
                    .on("start_time" -> ticket.start_time).on("end_time" -> ticket.end_time).on("note" -> ticket.note).on("note_en" -> ticket.note_en).on("enable" -> ticket.enable)
                    .on("min_buy" -> ticket.min_buy).on("max_buy" -> ticket.max_buy).on("count" -> ticket.count).on("quantity" -> ticket.quantity).on("no_COD" -> ticket.no_COD).on("no_promotion" -> ticket.no_promotion)
                    .executeInsert()

            result = id match {
                case Some(number) => number
                case None => -1
            }
        }

        result
    }

    def update(ticket: Ticket): Int = {
        db.withConnection { implicit connection =>
            SQL(
                """UPDATE ticket SET name={name}, event_id={event_id}, display_name={display_name}, display_name_en={display_name_en}, price={price}, start_time={start_time}, end_time={end_time}, note={note}, note_en={note_en}, enable={enable}, min_buy={min_buy}, max_buy={max_buy}, count={count}, quantity={quantity}, no_COD={no_COD}, no_promotion={no_promotion}
            WHERE ticket_id={ticket_id}""")
                .on("name" -> ticket.name).on("event_id" -> ticket.event_id).on("display_name" -> ticket.display_name).on("display_name_en" -> ticket.display_name_en).on("price" -> ticket.price).on("start_time" -> ticket.start_time).on("end_time" -> ticket.end_time)
                .on("note" -> ticket.note).on("note_en" -> ticket.note_en).on("enable" -> ticket.enable).on("ticket_id" -> ticket.ticket_id).on("quantity" -> ticket.quantity).on("min_buy" -> ticket.min_buy).on("max_buy" -> ticket.max_buy).on("count" -> ticket.count).on("no_COD" -> ticket.no_COD).on("no_promotion" -> ticket.no_promotion)
                .executeUpdate()
        }
    }

}
