package controllers

import javax.inject.Inject

import dao.TransactionDAO
import models.Transaction
import play.api.db.Database
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.mvc.{BaseController, ControllerComponents}


class TransactionController @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
  val transactionDAO: TransactionDAO = new TransactionDAO(db, controllerComponents)

  implicit val transactionWrites = new Writes[Transaction] {
    def writes(transaction: Transaction) = Json.obj(
      "transaction_id" -> transaction.transaction_id,
      "invoice_id" -> transaction.invoice_id,
      "payment_method" -> transaction.payment_method,
      "create_time" -> transaction.create_time,
      "finish_time" -> transaction.finish_time,
      "transaction_status" -> transaction.transaction_status,
      "username" -> transaction.username,
      "email" -> transaction.email,
      "phonenumber" -> transaction.phonenumber,
      "tickets_info" -> transaction.tickets_info,
      "seats_info" -> transaction.seats_info,
      "revenue_before" -> transaction.revenue_before,
      "revenue_discount" -> transaction.revenue_discount,
      "revenue_after" -> transaction.revenue_after,
      "voucher_code" -> transaction.voucher_code,
      "event_id" -> transaction.event_id,
      "event_name" -> transaction.event_name,
      "ticket_status" -> transaction.ticket_status,
      "note" -> transaction.note,
      "address" -> transaction.address
    )
  }

  implicit val transactionReads: Reads[Transaction] = (
      (JsPath  \ "transaction_id").read[Long] and
      (JsPath  \ "invoice_id").read[String] and
      (JsPath  \ "payment_method").read[Int] and
      (JsPath  \ "create_time").read[Long] and
      (JsPath  \ "finish_time").read[Long] and
      (JsPath  \ "transaction_status").read[Int] and
      (JsPath  \ "username").read[String] and
      (JsPath  \ "email").read[String] and
      (JsPath  \ "phonenumber").read[String] and
      (JsPath  \ "tickets_info").read[String] and
      (JsPath  \ "seats_info").read[String] and
      (JsPath  \ "revenue_before").read[Long] and
      (JsPath  \ "revenue_discount").read[Long] and
      (JsPath  \ "revenue_after").read[Long] and
      (JsPath  \ "voucher_code").read[String] and
      (JsPath  \ "event_id").read[Long] and
      (JsPath  \ "event_name").read[String] and
      (JsPath  \ "ticket_status").read[Int] and
      (JsPath  \ "note").read[String] and
      (JsPath  \ "address").read[String]
    )(Transaction.apply _)


  def create = Action {request =>
    var result: Long = -1
    request.body.asJson.map { json =>
      val event = json.validate[Transaction]
      var res = -1L

      event match {
        case s: JsSuccess[Transaction] =>
          val trans = s.get
          trans.create_time = System.currentTimeMillis / 1000
          res = transactionDAO.add(s.get)
          //update invoice id
        case e: JsError => println("Errors: " + JsError.toJson(e).toString())
      }
      Ok(res.toString)
    }.getOrElse{
      BadRequest("Expecting Json data")
    }
  }

  def getTransactions = Action { request =>
    val offset = request.queryString.get("offset").get(0).toLong
    val limit = request.queryString.get("limit").get(0).toLong
    val res = transactionDAO.getTransactions(offset, limit)
    Ok("")
  }

  def callbackPayment(): Unit = {

  }

  def createInvoiceNumber(): String = {
    "DP" + "%0.7d".format()
  }
}
