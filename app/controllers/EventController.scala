package controllers

import javax.inject.Inject

import dao.{EventDAO, VoucherGroupDAO, TicketDAO}
import helpers.JsonExt
import models._
import play.api.db.Database
import play.api.libs.json._
import play.api.mvc.{BaseController, ControllerComponents}
import play.api.libs.functional.syntax._

import scala.collection.mutable.ArrayBuffer

class EventController @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {
    val eventDAO: EventDAO = new EventDAO(db, controllerComponents)
    val ticketDAO: TicketDAO = new TicketDAO(db, controllerComponents)
    val voucherGroupDAO: VoucherGroupDAO = new VoucherGroupDAO(db, controllerComponents)

    implicit val eventBlockWrites = new Writes[EventBlock] {
        def writes(eventBlock: EventBlock) = Json.obj(
            "id" -> eventBlock.id,
            "short_name" -> eventBlock.short_name,
            "short_name_en" -> eventBlock.short_name_en,
            "title" -> eventBlock.title,
            "image" -> eventBlock.image,
            "content" -> eventBlock.content,
            "content_en" -> eventBlock.content_en
        )
    }

    implicit val eventBlockReads: Reads[EventBlock] = (
        (JsPath \ "id").read[Long] and
            (JsPath \ "short_name").read[String] and
            (JsPath \ "short_name_en").read[String] and
            (JsPath \ "title").read[String] and
            (JsPath \ "image").read[String] and
            (JsPath \ "content").read[String] and
            (JsPath \ "content_en").read[String]
        ) (EventBlock.apply _)


    def create = Action {
        Ok(views.html.event_index()(views.html.event_create("Purpose Media's Tool")))
    }

    def view = Action {
        val offset = 0
        val limit = 1000
        val res = eventDAO.getEvents(offset, limit)
        Ok(views.html.event_index()(views.html.event_view("Purpose Media's Tool", res)))
    }

    def edit(event_id: Long) = Action { request =>
        val res = eventDAO.getEventById(event_id)
        Ok(views.html.event_index()(views.html.event_edit("Purpose Media's Tool", res)))
    }

    def add = Action { request =>
        val event = JsonExt.gson.fromJson(request.body.asJson.get.toString, classOf[Event])
        val checkExistUrl = eventDAO.getEventByUrl(event.url)
        if (checkExistUrl == null) {
            val event_id = eventDAO.add(event)
            val tickets = JsonExt.gson.fromJson(event.tickets, classOf[Array[Ticket]])
            var newTickets = new ArrayBuffer[Ticket]
            for (ticket <- tickets) {
                ticket.event_id = event_id
                ticket.ticket_id = ticketDAO.add(ticket)
                newTickets = newTickets :+ ticket
            }
            event.tickets = JsonExt.toRawJson(newTickets)
            eventDAO.update(event)
            Ok(event_id.toString)
        } else {
            Ok("Error: duplicate url")
        }
    }

    def update = Action { request =>
        val event = JsonExt.gson.fromJson(request.body.asJson.get.toString, classOf[Event])
        val tickets = JsonExt.gson.fromJson(event.tickets, classOf[Array[Ticket]])
        val voucher_groups = JsonExt.gson.fromJson(event.voucher_groups, classOf[Array[VoucherGroup]])
        var newTickets = new ArrayBuffer[Ticket]
        var newVoucherGroups = new ArrayBuffer[VoucherGroup]
        for (ticket <- tickets) {
            if (ticket.ticket_id == -1)
                ticket.ticket_id = ticketDAO.add(ticket)
            else
                ticketDAO.update(ticket)

            newTickets = newTickets :+ ticket
        }

        for (voucher_group <- voucher_groups) {
            if (voucher_group.voucher_group_id == -1)
                voucher_group.voucher_group_id = voucherGroupDAO.add(voucher_group)
            else
                voucherGroupDAO.update(voucher_group)

            newVoucherGroups = newVoucherGroups :+ voucher_group
        }

        event.tickets = JsonExt.toRawJson(newTickets)
        event.voucher_groups = JsonExt.toRawJson(newVoucherGroups)

        Ok(eventDAO.update(event).toString)
    }

    def getEventByUrl(url: String) = Action { request =>
        val event = eventDAO.getEventByUrl(url)
        Ok(JsonExt.toRawJson(event))
    }

    def getEvents = Action { request =>
        val offset = request.queryString.get("offset").get(0).toLong
        val limit = request.queryString.get("limit").get(0).toLong
        val res = eventDAO.getEvents(offset, limit)
        Ok(views.html.event_view("Purpose Media's Tool", res))
    }
}
