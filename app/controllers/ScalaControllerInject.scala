package controllers

import javax.inject.Inject

import dao.EventDAO

import play.api.db._
import play.api.mvc._

class ScalaControllerInject @Inject()(db: Database, val controllerComponents: ControllerComponents) extends BaseController {

  def add = Action { request =>
    val eventDAO: EventDAO = new EventDAO(db, controllerComponents)
    var result: Long = -1
    request.body.asJson.map { json =>
      (json \ "name").asOpt[String].map { name =>
        //result = eventDAO.add(name)
        Ok(result.toString)
      }.getOrElse {
        BadRequest("Missing parameter [name]")
      }
    }.getOrElse{
      BadRequest("Expecting Json data")
    }
  }
}