package helpers

import com.google.gson._
import java.lang.reflect.{ParameterizedType, Type}
import com.google.gson.internal.$Gson$Types
import org.json.simple.parser.JSONParser

import scala.collection.mutable.ArrayBuffer
import scala.collection.JavaConversions._
import scala.collection.{immutable, mutable}

class ArrayBufferAdapter
    extends JsonSerializer[ArrayBuffer[_]]
        with JsonDeserializer[ArrayBuffer[_]] {

    def serialize(src: ArrayBuffer[_], typeOfSrc: Type, context: JsonSerializationContext): JsonElement = {
        val arr = new JsonArray()
        for (item <- src) {
            arr.add(context.serialize(item))
        }
        arr
    }

    def deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): ArrayBuffer[_] = {
        val arr = json.asInstanceOf[JsonArray]
        val result = new ArrayBuffer[Any]()
        val elType = typeOfT.asInstanceOf[ParameterizedType].getActualTypeArguments()(0)

        for (el <- arr) {
            result += context.deserialize(el, elType)
        }
        result
    }
}

class MapAdapter
    extends JsonSerializer[Map[_, _]]
        with JsonDeserializer[Map[_, _]] {

    def serialize(src: Map[_, _], typeOfSrc: Type, context: JsonSerializationContext): JsonElement = {
        val map = new JsonObject()
        for ((k, v) <- src) {
            val key = k.toString
            val value = context.serialize(v)

            map.add(key, value)
        }
        map
    }

    def deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Map[_, _] = {
        var map = Map[Any, Any]()
        val keyType = typeOfT.asInstanceOf[ParameterizedType].getActualTypeArguments()(1)

        for (entry <- json.asInstanceOf[JsonObject].entrySet()) {
            val k = entry.getKey.toString
            val v = entry.getValue
            val value = JsonExt.deserialize(context, keyType, v)

            map = map + ((k, value))
        }
        map
    }
}

class ImmutableMapAdapter
    extends JsonSerializer[immutable.HashMap[_, _]]
        with JsonDeserializer[immutable.Map[_, _]] {

    def serialize(src: immutable.HashMap[_, _], typeOfSrc: Type, context: JsonSerializationContext): JsonElement = {
        val map = new JsonObject()
        for ((k, v) <- src) {
            val key = k.toString
            val value = context.serialize(v)

            map.add(key, value)
        }
        map
    }

    def deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): immutable.HashMap[_, _] = {
        var map = immutable.HashMap[Any, Any]()
        val valueType = typeOfT.asInstanceOf[ParameterizedType].getActualTypeArguments()(1)

        for (entry <- json.asInstanceOf[JsonObject].entrySet()) {
            val k = entry.getKey
            val v = entry.getValue
            val value = JsonExt.deserialize(context, valueType, v)

            map = map + ((k, value))
        }
        map
    }
}

class MutableMapAdapter
    extends JsonSerializer[mutable.Map[_, _]]
        with JsonDeserializer[mutable.Map[_, _]] {

    def serialize(src: mutable.Map[_, _], typeOfSrc: Type, context: JsonSerializationContext): JsonElement = {
        val map = new JsonObject()
        try {
            for ((k, v) <- src) {
                val key = k.toString
                val value = context.serialize(v)

                map.add(key, value)
            }
            map
        }
        catch {
            case ex: Exception =>
                map
        }


    }

    def deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): mutable.Map[_, _] = {
        var map = mutable.Map[Any, Any]()
        val keyType = typeOfT.asInstanceOf[ParameterizedType].getActualTypeArguments()(1)

        for (entry <- json.asInstanceOf[JsonObject].entrySet()) {
            val k = entry.getKey
            val v = entry.getValue
            val value = JsonExt.deserialize(context, keyType, v)

            map = map + ((k, value))
        }
        map
    }
}

class MutableHashMapAdapter
    extends JsonSerializer[mutable.HashMap[_, _]]
        with JsonDeserializer[mutable.HashMap[_, _]] {

    def serialize(src: mutable.HashMap[_, _], typeOfSrc: Type, context: JsonSerializationContext): JsonElement = {
        val map = new JsonObject()
        for ((k, v) <- src) {
            val key = k.toString
            val value = context.serialize(v)

            map.add(key, value)
        }
        map
    }

    def deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): mutable.HashMap[_, _] = {
        var map = mutable.HashMap[Any, Any]()
        val keyType = typeOfT.asInstanceOf[ParameterizedType].getActualTypeArguments()(1)

        for (entry <- json.asInstanceOf[JsonObject].entrySet()) {
            val k = entry.getKey
            val v = entry.getValue
            val value = JsonExt.deserialize(context, keyType, v)

            map = map += (k -> value)
        }
        map
    }
}

class ListAdapter
    extends JsonSerializer[List[_]]
        with JsonDeserializer[List[_]] {

    def serialize(src: List[_], typeOfSrc: Type, context: JsonSerializationContext): JsonElement = {
        val array = new JsonArray()
        for (item <- src) {
            array.add(context.serialize(item))
        }
        array
    }

    def deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): List[_] = {
        val buf = new ArrayBuffer[Any]()
        val elType = typeOfT.asInstanceOf[ParameterizedType].getActualTypeArguments()(0)

        for (el <- json.asInstanceOf[JsonArray]) {
            val value = JsonExt.deserialize(context, elType, el)
            buf += value
        }

        buf.toList
    }
}

/*class BannerModelAdapter extends JsonSerializer[BannerModel] {
    def serialize(src: BannerModel, typeOfSrc: Type, context: JsonSerializationContext): JsonElement = {
        val jsonElement = context.serialize(src)
        jsonElement.getAsJsonObject.addProperty("encodeId", SecurityUtils.encode(src.id))
        return jsonElement
    }
}*/


object JsonExt{
    def deserialize(context: JsonDeserializationContext, elementType: Type, v: JsonElement): Any =
        elementType match {
            case x if x == classOf[Int] => v.getAsInt
            case x if x == classOf[Long] => v.getAsLong
            case x if x == classOf[String] => v.getAsString
            case x if x == classOf[Integer] => v.getAsInt
            case _ => context.deserialize(v, elementType)
        }


    val b = new GsonBuilder()
    b.registerTypeAdapter(classOf[ArrayBuffer[_]], new ArrayBufferAdapter())
    b.registerTypeAdapter(classOf[Map[_, _]], new MapAdapter())
    b.registerTypeAdapter(classOf[scala.collection.immutable.HashMap[_, _]], new ImmutableMapAdapter())
    b.registerTypeAdapter(classOf[mutable.Map[_, _]], new MutableMapAdapter())
    b.registerTypeAdapter(classOf[mutable.HashMap[_, _]], new MutableHashMapAdapter())
    b.registerTypeAdapter(classOf[List[_]], new ListAdapter())
    b.registerTypeAdapter(List().getClass, new ListAdapter())
    b.registerTypeAdapter(List(1,2,3).getClass, new ListAdapter())
    /*    b.registerTypeAdapter(classOf[ZaloSuggestAppResponse], new ZaloSuggestAppResponseDeserialize())
        b.registerTypeAdapter(classOf[ZaloSuggestSucc], new ZaloSuggestSuccDeserialize())
        b.registerTypeAdapter(classOf[BannerModel], new BannerModelAdapter())*/

    val gson = b.create()

    def toJson[T](instance: T): String = "{\"data\":" + s"${gson.toJson(instance)}}"

    def toRawJson[T](instance: T): String = gson.toJson(instance)
}
