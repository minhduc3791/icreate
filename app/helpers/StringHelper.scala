package helpers

import org.joda.time.DateTime
import play.api.libs.json.JsValue

object StringHelper {
  def toDate(timestamp: Long): Unit ={
    val date = new DateTime(timestamp)
    date.toString("dd-MMM-yyyy")
  }
}
