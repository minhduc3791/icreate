package models

case class Transaction (
                   var transaction_id: Long,
                   var invoice_id: String,
                   var payment_method: Int, //1: online - 2: cod - 3: offline
                   var create_time: Long,
                   var finish_time: Long,
                   var transaction_status: Int, // 1: created - 2: pending(update after call to online payment)
                                            // 3: succeed - 4: failed
                   var username: String,
                   var email: String,
                   var phonenumber: String,
                   var tickets_info: String,
                   var seats_info: String,
                   var revenue_before: Long,
                   var revenue_discount: Long,
                   var revenue_after: Long,
                   var voucher_code: String,
                   var event_id: Long,
                   var event_name: String,
                   var ticket_status: Int, // 1: delivered - 2: waiting for pick up - 3: canceled
                                       // 4: waiting for payment - 5: paid - 6: finished
                   var note: String,
                   var address: String
                   )/*{
  def this() = this(0, "", "")
}*/
