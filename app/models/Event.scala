package models


case class Event(
                    var event_id: Long,
                    var is_dual_language: Int,
                    var url: String,
                    var bigtime_url: String,
                    var name: String,
                    var start_time: Long,
                    var end_time: Long,
                    var display_time: String,
                    var logo: String,
                    var banner: String,
                    var intro: String,
                    var instagram: String,
                    var description: String,
                    var description_en: String,
                    var event_blocks: String,
                    var tickets: String,
                    var voucher_groups: String,
                    var address: String,
                    var address_en: String,
                    var location: String,
                    var sponsor_name: String,
                    var sponsor_image: String,
                    var list_payment_method: String
                ) /*{
  def this() = this(0, "", "", "", "", "", "", "", "", "")
}*/


/*( var id: Long = 0,
                 var name: String = "",
                 var url: String = ""
                 var start_time: String = "",
                 var end_time: String = "",
                 var logo: String = "",
                 var banner: String = "",
                 var description: String = "",
                 var _event_blocks: List[EventBlock] = Nil,
                 var event_blocks: String = "",
                 var _required_info: List[RequiredInfo] = Nil,
                 var required_info: String = "",
                 var _tickets: List[Ticket] = Nil,
                 var tickets: String = "",

               )*/


