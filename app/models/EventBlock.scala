package models

case class EventBlock (
                  var id: Long,
                  var short_name: String,
                  var short_name_en: String,
                  var title: String,
                  var image: String,
                  var content: String,
                  var content_en: String
                 )/*{
  def this() = this(0, "", "", "")
}*/
