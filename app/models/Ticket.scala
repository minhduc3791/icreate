package models

case class Ticket(
             var ticket_id: Long,
             var event_id: Long,
             var name: String,
             var display_name: String,
             var display_name_en: String,
             var price: Long,
             var start_time: Long,
             var end_time:Long,
             var note: String,
             var note_en: String,
             var enable: Int,
             var min_buy: Int,
             var max_buy: Int,
             var count: Int,
             var quantity: Int,
             var no_COD: Int,
             var no_promotion: Int
            )/*{
  def this() = this(0, "", 0, "", "", "")
}*/
